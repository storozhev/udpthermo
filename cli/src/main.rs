use std::io::{BufRead, BufReader, ErrorKind::InvalidInput};
use std::{
    error::Error,
    net::{SocketAddr, UdpSocket},
};

const ADDR_TO_CONN: &str = "127.0.0.1:3400";
const PORT_TO_BIND: u16 = 3401;

fn main() -> std::io::Result<()> {
    let mut port_to_bind = PORT_TO_BIND;
    let socket = loop {
        match UdpSocket::bind(SocketAddr::from(([127, 0, 0, 1], port_to_bind))) {
            Ok(s) => break s,
            Err(_) => port_to_bind += 1,
        }
    };

    socket.connect(ADDR_TO_CONN)?;

    loop {
        println!("please enter temperature (integer with no separators)...");

        let br = BufReader::new(std::io::stdin());
        let cur_temp = get_temperature(br).map_err(|_| InvalidInput)?;

        socket.send(&cur_temp.to_be_bytes())?;
    }
}

fn get_temperature(mut r: impl BufRead) -> Result<i32, Box<dyn Error>> {
    let mut buf = String::new();
    r.read_line(&mut buf)?;

    let tmp: i32 = buf[..buf.len() - 1].parse()?;

    Ok(tmp)
}

#[cfg(test)]
mod test {
    use super::*;
    use std::io::Cursor;

    #[test]
    fn get_temperature_ok() {
        let got_temp = get_temperature(Cursor::new("42\n"));

        assert!(got_temp.is_ok());
        assert_eq!(42, got_temp.unwrap());
    }

    #[test]
    fn get_temperature_fail() {
        let got_temp = get_temperature(Cursor::new("foo\n"));

        assert!(got_temp.is_err());
    }
}
