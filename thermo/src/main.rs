use std::net::UdpSocket;
use std::thread;

#[derive(Default, Debug)]
struct Thermometer(i32);

impl Thermometer {
    pub fn set_temperature(&mut self, value: i32) {
        self.0 = value;
    }
}

const ADDR_TO_BIND: &str = "127.0.0.1:3400";

fn main() -> std::io::Result<()> {
    let socket = UdpSocket::bind(ADDR_TO_BIND)?;
    let mut thermometer = Thermometer::default();

    let mut buf = [0; 4];

    let jh = thread::spawn(move || {
        while socket.recv(&mut buf).is_ok() {
            let new_value = i32::from_be_bytes(buf);
            thermometer.set_temperature(new_value);
            println!("new thermometer state: {:?}", thermometer);
        }
    });

    jh.join().unwrap();

    Ok(())
}
